import { useEffect, useState } from 'react';
import '../src/style.css';

export const AppCompleted = () => {
  const [hrivna, setHrivna] = useState(null);
  const [euro, setEuro] = useState(0);
  const [usd, setUsd] = useState(0);

  const hrivnaHandler = (event) => {
    const { value } = event.target;
    if (value === '-' || value < 0) {
      alert('Please insert the correct value');
    } else {
      setHrivna(value);
    }
  };

  const euroHandler = () => {
    const euro = hrivna * 33;
    setEuro(euro);
  };

  const usdHandler = () => {
    setUsd(hrivna * 29);
  };

  useEffect(() => {
    euroHandler();
    usdHandler();
  }, [hrivna]);

  return (
    <div className="currencyChanger">
      <h1>Currency</h1>
      <p>Insert the currency in Hrivnas</p>
      <input type="text" onChange={hrivnaHandler} />
      <div>Euro: {euro}</div>
      <div>USD: {usd}</div>
    </div>
  );
};
